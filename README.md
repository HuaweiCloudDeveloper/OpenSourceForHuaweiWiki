本项目是`开源for Huawei`的参考文档源码。 

# 如何使用

* 本地下载源码：

  ```
  git clone https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiWiki.git
  ```
  
* 参考[MkDocs安装指南](https://www.mkdocs.org/#installation), 安装 Python、Pip、MkDocs 
* 安装主题: `pip install mkdocs-material`

* 启动本地WEB服务器并查看文档内容：

  ```
  cd zh_CN
  mkdocs serve
  ```

* 通过浏览器 `http://127.0.0.1:8000/index.html` 查看文档内容: 

  ![](zh_CN/docs/assets/wiki-home.png)
