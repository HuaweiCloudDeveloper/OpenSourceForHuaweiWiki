# 体验项目介绍

体验项目基于 [OpenSourceForHuaweiDemoGo](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo) , 它使用 go-zero 框架做 RESTful 服务，数据层使用 GORM 连接 GaussDB 数据库。

[go-zero](https://go-zero.dev/) 是一个集成了各种工程实践的 web 和 rpc 框架，它通过弹性设计保障了大并发服务端的稳定性，经受了充分的实战检验。

[GORM](https://gorm.io) 是一个强大的 Go 编程语言中的 ORM 库，具有简单易用、多数据库支持、自动迁移、事务支持和强大的查询功能等特点。

华为云数据库 [GaussDB](https://support.huaweicloud.com/gaussdb/index.html) 是华为自主创新研发的分布式关系型数据库。该产品具备企业级复杂事务混合负载能力，同时支持分布式事务，同城跨AZ部署，数据0丢失，支持1000+的扩展能力，PB级海量存储。同时拥有云上高可用，高可靠，高安全，弹性伸缩，一键部署，快速备份恢复，监控告警等关键能力。

## 关于 openGauss 和 GaussDB 驱动的说明

[openGauss](https://gitee.com/opengauss/openGauss-connector-go-pq)

[获取GaussDB驱动包](https://support.huaweicloud.com/centralized-devg-v8-gaussdb/gaussdb-42-1836.html)

两者在使用上的区别：GaussDB 驱动包为了兼容 ORM 框架，在创建数据库连接时注册的驱动名称兼容“postgres”和“postgresql”，而 openGauss 在连接数据库时注册的驱动名称是“opengauss”。[兼容性参考](https://support.huaweicloud.com/centralized-devg-v8-gaussdb/gaussdb-42-1649.html)

代码示例：
```Go
package main

import (
    // go.mod 通过 replace openGauss-connector-go-pq 替换成本地下载的GaussDB驱动包
	_ "gitee.com/opengauss/openGauss-connector-go-pq"
    "gorm.io/driver/postgres"
    "gorm.io/gorm"
)

func main() {
	dsn := "host=127.0.0.1 user=test password=123456 dbname=test port=8000 sslmode=disable"
    db, err := gorm.Open(postgres.New(postgres.Config{DriverName: "postgres", DSN: dsn}), &gorm.Config{})
    if err != nil {
    	panic(err)
    }
    // TODO ...
}
```

## 下载和本地运行示例项目

这里介绍如何在本地运行示例项目。您也可以直接参考 [在华为云部署示例项目](cicd-pipeline.md) 。

git clone https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo.git

cd OpenSourceForHuaweiDemoGo

1.下载 GaussDB 驱动包：

https://support.huaweicloud.com/centralized-devg-v8-gaussdb/gaussdb-42-1836.html

2.修改 go.mod 文件，添加：

replace gitee.com/opengauss/openGauss-connector-go-pq => ./openGauss-connector-go-pq

3.初始化数据库：

在华为云 GaussDB 数据库终端执行 apps/user/model/user.sql 语句，创建用户表。

4.数据库需要配置GaussDB连接信息：

(1)在项目配置文件中 apps/user/rpc/etc/user-rpc.yaml 的GaussDB下配置DataSource连接信息，在Nacos下配置Nacos配置信息：

(2)或者使用环境变量 DataSource 配置；Nacos 也一样；

5.运行示例： 
* 先运行 rpc 服务，`go run apps/user/rpc/user.go`
* rpc 服务启动后再运行 api 的 `go run apps/user/api/user.go`
* 或者也可以在 OpenSourceForHuaweiDemoGo 下使用 Makefile 运行





