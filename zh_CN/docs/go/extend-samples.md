# 为体验项目扩展功能

* [go-zero官方文档](https://go-zero.dev)

## rpc层
```proto
syntax = "proto3";
package user;
option go_package = "./user";

message Req {
  string Ping = 1;
}

message Resp {
  string Pong = 1;
}

service User {
    rpc Test(Req) returns(Resp)；
}
```
1. 使用goctl rpc生成pb.go文件，参考[goctl命令](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/blob/main/bin/exec.sh)
2. 配置svc，注册model
```go
package svc

import (
	"database/sql"
	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/apps/user/model"
	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/apps/user/rpc/internal/config"
	_ "gitee.com/opengauss/openGauss-connector-go-pq"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type ServiceContext struct {
	Config config.Config

	GaussDBConn *sql.DB
	UserModel   model.UserModel
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn := sqlx.NewSqlConn("opengauss", c.GaussDB.DataSource)

	return &ServiceContext{
		Config:    c,
		UserModel: model.NewUserModel(conn),
	}
}
```
3. 编写logic，调用model，编写业务逻辑
```go
package logic

import (
	"context"
	"fmt"

	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/apps/user/rpc/internal/svc"
	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/apps/user/rpc/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type TestLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewTestLogic(ctx context.Context, svcCtx *svc.ServiceContext) *TestLogic {
	return &TestLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *TestLogic) Test(in *user.Req) (*user.Res, error) {

	return &user.Response{
		Pong: fmt.Sprintf("Response: %v", in.Ping),
	}, nil
}
```
可以使用[grpcui](https://github.com/fullstorydev/grpcui)进行测试

## api层
```api
type (
    Req {
        Req string `json:"req"`
    }
    Resp {
        Resp string `json:"resp"`
    }
)

@server (
    prefix: v1/user
    group:  user
)
service user {
    @doc "Test"
    @handler test
    get /test (Req) returns (Resp)
}
```
1. 使用goctl api生成代码：参考[goctl命令](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/blob/main/bin/exec.sh)
2. api层配置svc，注册rpc客户端
```go
package svc

import (
	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/apps/user/api/internal/config"
	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/apps/user/rpc/userClient"
	"github.com/zeromicro/go-zero/zrpc"
)

type ServiceContext struct {
	Config config.Config

	UserRpc userClient.User
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config:  c,
		UserRpc: userClient.NewUser(zrpc.MustNewClient(c.UserRpc)),
	}
}
```
3. 在logic层调用rpc方法，实现接口逻辑
```go
package user

import (
	"context"
	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/apps/user/rpc/user"
	"github.com/jinzhu/copier"

	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/apps/user/api/internal/svc"
	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/apps/user/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type TestLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

// 测试接口
func NewTestLogic(ctx context.Context, svcCtx *svc.ServiceContext) *TestLogic {
	return &TestLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *TestLogic) Test(req *types.Req) (resp *types.Resp, err error) {
	testResp, err := l.svcCtx.UserRpc.Test(l.ctx, &user.Req{
		Ping: req.Req,
	})
	if err != nil {
		return nil, err
	}
	var res types.TestResp
	copier.Copy(&res, testResp)

	return &res, nil
}

```
4. 返回restful api

5. 生成接口文档参考[goctl命令](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/blob/main/bin/exec.sh)

## model层
如果现有model数据操作接口无法满足，则添加新的数据操作接口并实现新添加的操作数据接口

