# 在华为云部署体验项目

本文介绍如何使用 CodeArts 完成示例项目的构建与部署，实现持续交付。本文采用的部署方式为 CCE 部署，适用于容器化部署场景。如果您希望使用传统软件包部署方法，请参考 [使用 CodeArts 快速搭建基于 ECS 部署的代码开发流水线](https://support.huaweicloud.com/qs-devcloud/devcloud_qs_0001.html)
。

本文以部署 [OpenSourceForHuaweiDemoGo](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo) 为例，描述如何使用 CodeArts 部署示例项目。

## 前提条件

请参考 [适配版本说明](../resource-versions.md) 准备好资源， 参考 [示例项目说明](intro-samples.md) 初始化数据库。

## 新建项目

项目是使用 CodeArts 各服务的基础，创建项目后才能完成后续操作。

* 打开`软件开发生产线（CodeArts）` -> `前往工作台` -> `新建项目` 。
* 选择 `Scrum`，输入项目名称 `OpenSourceForHuaweiDemoGo`。

## 构建并推送镜像

**通过编译构建任务将软件的源代码编译成镜像，并把镜像推送归档到容器镜像服务（SWR）中。**

* 打开 `持续交付` -> `编译构建` -> `新建任务`，配置构建任务。
* 服务扩展没有的话就新建。

![](resource/cicd-build-basic.png)

下一步选择模板，使用 `系统模板` -> `Go语言` 。

* 配置构建步骤

  构建步骤保留 `构建环境配置`、`代码下载配置`、`Go语言构建`、`制作镜像并推送到SWR仓库`。

  1.构建环境配置：构建环境主机类型，选择 `鲲鹏（ARM）服务器`，执行主机选择 `内置执行机`。

  2.代码下载配置：缺省。

  3.Go语言构建：工具版本 `go-1.22` 或者以上版本，命令框内可以全部删除。

  4.制作镜像并推送到SWR仓库：
  
  5.这里需要分别制作制作rpc和api的镜像:

  ![](resource/go-build.png)
  ![](resource/build-rpc.png)
  ![](resource/build-api.png)

完成配置，单击“保存并执行”。等待任务执行完毕，在镜像仓库会生成这个微服务的镜像。镜像内容可以通过 `容器镜像服务SWR` -> `我的镜像` 进行查看。<br>
可以看到我们刚刚构建的`api`和`rpc`镜像。


## 配置nacos注册配置中心
进入[微服务引擎控制台](https://console.huaweicloud.com/cse2)确认授权
* 进入`注册配置中心` -> `购买注册配置中心` -> 参考 [适配版本说明](../resource-versions.md) 中的 `微服务引擎CSE` 配置。 
* 在注册配置中心，查看创建的 `实例名称/ID` -> 进入查看实例信息。
* 这里的内网地址就是后面我们需要用到的注册中心地址。

  ![](resource/nacos-info.png)

## 创建工作负载

**在云容器引擎（CCE）中创建无状态负载（Deployment），部署这个微服务。**

1.首先部署`rpc`服务（server端）<br>
给容器添加环境变量:

* DataSource 是 GaussDB 数据库连接字符串信息：(DataSource中GaussDB的IP为内网IP)
* IP是上面Nacos实例信息中的内网地址：


![](resource/rpc-info.png)

服务类型选择 `集群内访问`：

![](resource/cluster-ip.png)

2.然后部署`api`服务（client端）<br>
给容器添加环境变量:

* NACOS_IP 是 Nacos 注册中心的内网地址：
* ENDPOINTS 是 `rpc` 服务集群内访问的访问地址：

![](resource/api-info.png)

服务类型选择 ELB 负载均衡，这样就可以通过公网 IP 访问到这个服务：

![](resource/elb.png)

3.`api`和`rpc`部署成功如下：
* CCE 无状态负载：
![](resource/deploy.png)

* Nacos 服务管理：
![](resource/nacos-register.png)

## 查看部署结果并进行接口验证

我们可以使用 Postman 测试服务接口是否正常：
![](resource/api-test-health.png)
![](resource/api-test.png)
![](resource/api-test2.png)

在任务验收的时候，需要提供 REST 接口测试结果的截图。

## 后续工作

您可以使用 CodeArts `部署镜像`、`流水线`等功能将上述构建、部署过程实现自动化，在代码变更的时候，一键式触发应用部署更新。 本文不再描述相关步骤。 


