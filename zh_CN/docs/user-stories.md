# 适配开发案例集

## Apache ServiceComb集成MyBatis使用GaussDB开源体验任务

* 适配心得: [Apache ServiceComb集成MyBatis使用GaussDB开源体验任务过程](https://bbs.huaweicloud.com/blogs/442452)

## Spring Data JPA适配GaussDB开源验证任务

* 适配心得: [Spring Data JPA适配GaussDB开源验证任务心得](https://bbs.huaweicloud.com/blogs/438331)

## Mybatis适配GaussDB开源验证任务

* 适配心得: [Mybatis适配华为云&Gaussdb](https://bbs.huaweicloud.com/blogs/437134)

## Mybatis Plus适配GaussDB开源验证任务

* 适配心得: [Mybatis plus适配华为云&Gaussdb](https://bbs.huaweicloud.com/blogs/438784)

## Sequelize适配GaussDB开源验证任务

* 适配心得: [Sequelize适配GaussDB开源开发过程](https://bbs.huaweicloud.com/blogs/437037)

## Qwen2适配昇腾开源验证任务

* 适配心得: [Qwen2适配Ascend NPU、Kunpeng CPU 以及 OpenEuler 操作系统开源验证任务心得](https://bbs.huaweicloud.com/blogs/438997)

## Apache Seata、Apache ServiceComb分布式事务功能集成验证

* 适配心得: [Apache Seata、GaussDB、ServiceComb分布式事务功能集成验证](https://bbs.huaweicloud.com/blogs/440589)

## HAProxy代理微服务和GaussDB

* 适配心得: [HAProxy代理微服务和GaussDB请求验证任务心得](https://bbs.huaweicloud.com/blogs/441986)

## Express使用GaussDB体验心得

* 适配心得: [Express使用GaussDB体验心得](https://bbs.huaweicloud.com/blogs/444455)
