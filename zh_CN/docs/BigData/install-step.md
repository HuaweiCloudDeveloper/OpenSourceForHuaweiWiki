# 在华为云部署体验项目


## 环境准备
一.ECS环境准备

    本集群需要准备三台ECS主机
   
    1.修改服务器名称（分别在三台机器上执行）
        hostnamectl set-hostname hadoop102
        hostnamectl set-hostname hadoop103
        hostnamectl set-hostname hadoop104
    2.配置集群间的地址映射
        vim /etc/hosts
        输入以下内容（替换为自己机器的IP）：
        10.xxx.xx.200 hadoop102
        10.xxx.xx.201 hadoop103
        10.xxx.xx.202 hadoop104
    3.配置免密登录
        a.首先，在三台机器上都执行 ssh hadoop102；以在/root下生成.ssh文件夹，进入到.ssh目录下；
            cd /root/.ssh
        b.在三台机器上都执行
            ssh-keygen -t rsa
        注：按三次enter执行完成，生成公钥和私钥

![](resource/ssh-keygen.png)

        c.将公钥分发到所有的机器上，以下命令所有机器都要执行
            ssh-copy-id hadoop102
            ssh-copy-id hadoop103
            ssh-copy-id hadoop104
            免密登录配置完成。
   
二.JDK环境准备

    安装JDK
    本次Flink安装版本为Flink1.18,官方推荐安装JDK11
[下载地址](https://www.oracle.com/cn/java/technologies/downloads/#java11)


![](resource/JDK11.png)


       a. 上传文件到/opt目录下
           cd /opt    
       b. 解压文件
           tar -zxvf jdk-11.0.25_linux-x64_bin.tar.gz
       c. 配置环境变量
          创建一个自己的环境变量文件(YOUR_NAME可自行替换)；
            vim /etc/profile.d/YOUR_NAEM.sh
           添加如下内容：
             #JAVA_HOME
             export JAVA_HOME=/opt/jdk-11.0.25
             export PATH=$PATH:$JAVA_HOME/bin
             注：添加完成后，按esc，输入:wq；
             刷新环境变量，使新环境变量生效；
             source /etc/profile
        d. 验证是否成功
             java -version
             注:显示版本信息即成功。


![](resource/JAVA-version.png)

三.Flink环境

        下载地址：
        https://www.apache.org/dyn/closer.lua/flink/flink-1.18.1/flink-1.18.1-bin-scala_2.12.tgz

![](resource/flink-download.png)


        a.上传到/opt目录下
            cd /opt
        b.解压安装包
            tar -zxvf /opt/flink-1.18.1-bin-scala_2.12.tgz
        c.修改配置信息
            cd /opt/flink-1.18.1/conf
            
            #修改flink-conf.yaml
               vim flink-conf.yaml
               
            # JobManager 节点地址
                jobmanager.rpc.address: hadoop102
                jobmanager.bind-host: 0.0.0.0
                rest.address: hadoop02
                rest.bind-address: 0.0.0.0
            # Taskmanager节点地址
                taskmanager.bind-host: 0.0.0.0
                taskmanager.host: hadoop102
            # 按esc，输入:wq保存退出；
            
            # 配置masters
                vim masters
                修改为：hadoop102:8081
            # 按esc，输入:wq保存退出；
            
            # 配置workers，删除localhost；注意：不可以有多余字符
                hadoop102
                hadoop103
                hadoop104
            # 按esc，输入:wq保存退出；
        d.分发安装包到其他两台机器上
            scp -r /opt/flink-1.18.1 user@hadoop103:/opt
            scp -r /opt/flink-1.18.1 user@hadoop104:/opt
        e.修改hadoop103、hadoop104的配置文件
            vim /opt/flink-1.18.1/conf/flink-conf.yaml
            # hadoop103的Taskmanager节点地址
                taskmanager.bind-host: 0.0.0.0
                taskmanager.host: hadoop103
              # 按esc，输入:wq保存退出；
            
            # hadoop104的Taskmanager节点地址
                taskmanager.bind-host: 0.0.0.0
                taskmanager.host: hadoop104
               # 按esc，输入:wq保存退出；

        启动集群
        到hadoop102机器的目录下
            cd /opt/flink-1.18.1
        执行bin/start-cluster.sh


![](resource/flink-start.png)


      启动成功！
      打开浏览器，进入Web UI
      http://localhosts:8081/
      localhosts替换自己的公网IP

![](resource/flink-overview.png)


        说明
        购买云服务器需要到控制台开放相应端口权限才可以正常使用，如Web UI的8081；
        集群之间的通信端口也要放开，在如下位置查询和更改；
        vim /opt/flink-1.18.1/conf/flink-conf.yaml

## 数据准备
  1.MySql：

    首先到华为云购买mysql服务；

![](resource/mysql_1.png)

![](resource/mysql_2.png)


    购买默认配置即可，记录好root密码

    命令行登录：
     mysql -u root -p -h <your-cloud-db-host> -P <port>

    创建新的数据库：
    CREATE DATABASE your_database_name;

    跳转到新创建的数据库
    USE your_database_name;

    创建一张表
    CREATE TABLE your_table_name (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(50),
        gender VARCHAR(10)
    );

    生成200条随机数据脚本
    -- 插入 200 条随机数据
    INSERT INTO your_table_name (name, gender)
    SELECT
      CONCAT('Name_', FLOOR(RAND() * 1000)) AS name, -- 生成随机名字
      CASE
        WHEN RAND() < 0.5 THEN 'Male'
        ELSE 'Female'
      END AS gender -- 随机生成性别
    FROM
      (SELECT 1 FROM information_schema.tables LIMIT 200) AS tmp;

  2.GaussDB

    到华为云购买服务;

![](resource/buy-gaussdb.png)


    同Mysql，牢记root密码

    登录数据库
       gsql -U root -W

    创建数据库 
       CREATE DATABASE my_database;

    切换到新创建的数据库
       use my_database;

    按照MySQL中的表结构在GaussDB中创建对应的表。
    CREATE TABLE person_info ( 
        id SERIAL PRIMARY KEY, -- 对应MySQL中的AUTO_INCREMENT
        name VARCHAR(50),
        gender VARCHAR(10)
    );

## 将项目程序打成jar包部署到flink

    将以下代码需要打成jar包上传至$FLINK_HOME/lib 目录下

[大数据体验任务DEMO](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoBigData/overview)

    将以上代码需要打成jar包上传至$FLINK_HOME/lib 目录下

    说明: 脚本采用flinkcdc，初次执行会全量同步;可以通过mvn clean package命令将程序打成jar包。将jar包从flink WebUI界面 
    或者 命令行方式上传至flink集群;在flink WebUI查看任务是否执行成功，执行成功后去Gauss里面查看刚刚新增的数据是否同步过来。
