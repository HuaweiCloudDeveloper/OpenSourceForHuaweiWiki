# 体验项目介绍

[OpenSourceForHuaweiDemoSpringCloud](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoSpringCloud)体验项目基于[Spring Cloud Huawei](https://github.com/huaweicloud/spring-cloud-huawei) , 参考[Apache ServiceComb Fence](https://github.com/apache/servicecomb-fence)的功能设计，为开发者提供了一个开箱即用的微服务架构体验项目。它可以帮助开发者快速构建包含微服务后端、微服务前端和基础原子服务的项目工程。

其核心设计包括韧性架构、安全认证、过载防护等方面，它主要由如下几个微服务组成：

* edge-service: 微服务网关。微服务网关负责接收用户的请求，并将请求转发给对应的微服务前端和微服务后端进行处理。微服务网关还负责认证鉴权、过载防护等方面的系统功能。
* authentication-server: 认证服务。认证服务提供基于Oauth2的认证能力，实现了基础的用户、角色管理，认证和授权等功能。
* resource-server: 资源服务。资源服务是一个示例项目，用户新开发的服务都属于资源服务，这个服务可以作为新增服务的模板。 资源服务实现了一些基础的鉴权功能，提供基于Yaml配置和Java Annotation声明的权限管理机制。 
* admin-service: 微服务管理服务。微服务管理服务提供微服务管理和系统运维功能。包括管理和查看服务列表、管理和查看服务配置、系统问题分析和定位能力。 
* admin-website: 微服务管理服务的前端。这是一个基于[OpenTiny](https://opentiny.design/) 开发的静态页面服务，实现微服务管理服务的Web前端。 它可以作为用户新增前端的模板，提供了基础的Web前端开发框架，包括菜单、导航等方便阅读和扩展的代码框架。

体验项目依赖CSE微服务平台和数据库GuassDB。其部署示意图如下：

![img.png](assets/bushu.png)


体验项目遵循*约定优于配置*原则，定义了日志配置、Web配置、路由配置、代码结构等相关规范，以更加简洁的实现可观测性、过载防护等功能。 

## 下载和本地运行示例项目

这里介绍如何在本地运行示例项目。您也可以直接参考 [在华为云部署示例项目](cicd-spring-cloud.md) 。

```
git clone https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoSpringCloud.git
cd OpenSourceForHuaweiDemoSpringCloud
mvn clean install
```

* 初始化数据库
  authentication-server依赖数据库，找到 src/resource/sql/user.sql，在数据库执行该初始化脚本。
* 启动和运行
  可以使用 `build_and_run.bat` 脚本启动项目，也可以使用 IDEA 打开 Fence工程，启动Fence的每个微服务。
* 进入管理界面。
  输入：http://localhost:12000/ui/admin/ , 登录系统后可以看到如下界面
  ![](assets/fence-home.png)
