# 在华为云部署体验项目
本文介绍如何使用 CodeArts 完成示例项目的构建与部署，实现持续交付。本文采用的部署方式为 CCE 部署，适用于容器化部署场景。如果您希望使用传统软件包部署方法，请参考 使用 [CodeArts 快速搭建基于 ECS 部署的代码开发流水线](https://support.huaweicloud.com/qs-devcloud/devcloud_qs_0001.html) 。

本文以部署 [OpenSourceForHuaweiDemoSpringCloud](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoSpringCloud) 为例，描述如何使用 CodeArts 部署示例项目。


## 前提条件
请参考 [适配版本说明](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiWiki/blob/main/zh_CN/docs/resource-versions.md) 准备好资源。
请先购买好VPC、Gaussdb，创建安全组、CCE。**注意所购买的服务，都必须在一个vpc下，否则网络调用不同，所购买的服务都在一个区域内，否则调用不同，例如 华南广州**

## 购买Code Arts
![img.png](assets/purchase-code-arts.png)
选择区域，点击确定。

## 购买微服务引擎 CSE
![img_2.png](assets/img_2.png)
copy 这两个地址，后面会用到
![img_3.png](assets/img_3.png)

## 新建项目
项目是使用 CodeArts 各服务的基础，创建项目后才能完成后续操作。

打开 `软件开发生产线（CodeArts）` -> `前往工作台` -> `新建项目` 。
选择 `Scrum`，输入项目名称 `Spring Cloud Demo`。
## 构建并推送镜像

**通过编译构建任务将软件的源代码编译成镜像，并把镜像推送归档到容器镜像服务（SWR）中。**

* 打开 `持续交付` -> `编译构建` -> `新建任务`，配置构建任务。

![img.png](assets/img.png)

下一步选择模板，使用 `系统模板` -> `Maven` 。

* 配置构建步骤

  构建步骤保留 `构建环境配置`、`代码下载配置`、`maven构建`、`制作镜像并推送到SWR仓库`。

  构建环境配置：构建环境主机类型，选择 `鲲鹏（ARM）服务器`，执行主机选择 `内置执行机`。

  代码下载配置：缺省。

  maven构建：工具版本 `maven3.8.5-jdk17` 或者以上版本, 命令：mvn package -Dmaven.test.skip=true -U -B -Pcse。
  setting配置选择 `国内站点`, 仓库地址配置为 `https://mirrors.huaweicloud.com/repository/maven/` 。

  制作镜像并推送到SWR仓库：需要增加 `edge-service`、`authentication-server`、`resource-server`、`admin-service`、`admin-website` 5个条目。

![](assets/cicd-build-steps.png)

完成配置，单击“保存并执行”。等待任务执行完毕，在镜像仓库会生成上述5个微服务的镜像。镜像内容可以通过 `容器镜像服务` -> `我的镜像` 进行查看。

## 创建节点
在CCE的页面需要先购买节点（ECS），购买后，方可调度。建议购买鲲鹏，因为构建的docker镜像是arm架构的。
![img_4.png](assets/img_4.png)

## 创建负载

**在云容器引擎（CCE）中创建无状态负载（Deployment），部署 `edge-service`、`authentication-server`、`resource-server`、`admin-service`
、`admin-website` 5个微服务。**

### 创建 edge-service

* 进入 `云容器引擎（CCE)` 控制台，点击准备工作中已经购买的集群，进入详情页。进入 `工作负载` -> `创建工作负载`。
* 工作负载基础信息：

| 配置类别 | 配置项             | 值                    |
|------|-----------------|----------------------|
| 基本信息 | 负载类型            | 无状态负载                |
| 基本信息 | 负载名称            | edge-service         |
| 基本信息 | 实例数量            | 1                    |
| 容器配置 | 基本信息-镜像名称       | 从我的镜像选择 edge-service |
| 容器配置 | 基本信息-更新策略       | 总是拉取镜像               |
| 容器配置 | 基本信息-镜像版本       | v1.1                 |
| 容器配置 | 环境变量 | 填写需要修改的环境变量          |
| 服务配置 | 访问类型            | 负载均衡                 |
| 服务配置 | 负载均衡器           | 共享型，创建一个共享型负载均衡器     |
| 服务配置 | 容器端口            | 12000                |
| 服务配置 | 服务端口            | 12000                |

环境变量，这边需要修改的如下：
```
spring.cloud.servicecomb.discovery.address=CSE引擎的服务发现的地址
spring.cloud.servicecomb.config.serverAddr=CSE引擎的配置中心的的地址
```

### 创建 `authentication-server`、`resource-server`、`admin-service`、`admin-website`

* 进入 `云容器引擎（CCE)` 控制台，点击准备工作中已经购买的集群，进入详情页。进入 `工作负载` -> `创建工作负载`。
* 工作负载基础信息：

| 配置类别 | 配置项              | 值                                                                                       |
|------|------------------|-----------------------------------------------------------------------------------------|
| 基本信息 | 负载类型             | 无状态负载                                                                                   |
| 基本信息 | 负载名称             | 具体名称                                                                                    |
| 基本信息 | 实例数量             | 1                                                                                       |
| 容器配置 | 基本信息-镜像名称        | 从我的镜像选择具体镜像                                                                             |
| 容器配置 | 基本信息-更新策略        | 总是拉取镜像                                                                                  |
| 容器配置 | 基本信息-镜像版本        | v1.1                                                                                    |
| 容器配置 | 环境变量  | 填写需要修改的环境变量                                                             |
| 容器配置 | 环境变量-db_url      | GaussDB URL，比如：jdbc:opengauss://ip:port/postgres?currentSchema=authentication_server_db |
| 容器配置 | 环境变量-db_username | GaussDB 用户名                                                                             |
| 容器配置 | 环境变量-db_password | GaussDB 密码                                                                              |

环境变量，这边需要修改的如下：
```
spring.cloud.servicecomb.discovery.address=CSE引擎的服务发现的地址
spring.cloud.servicecomb.config.serverAddr=CSE引擎的配置中心的的地址
```
只有authentication-server才需要配置数据库信息，数据库相关脚本见user.sql


> 注意：请查看具体微服务的 `application.yml` 文件，了解数据库环境变量的名称。不同的服务可能会有差异。 如果名称为 `db.url`，那么环境变量需要指定为 `db_url`。 建议配置文件名称使用大写的方式，和环境变量命名规范一致，比如 `DB_URL`。

### 查看部署结果和进行接口验证

打开新的浏览器页面，输入 `http://{IP}:12000/ui/admin/` 进入登录页面，其中IP为edge-service负载均衡的公网地址。登录后会出现如下界面：

![img_6.png](assets/img_6.png)![](assets/fence-home.png)

打开 `系统运维` -> `接口测试` 菜单，可以对开源任务增加的 REST 接口进行测试。

![img_5.png](assets/img_5.png)

在任务验收的时候，需要提供 REST 接口测试结果的截图。

> 注意： URL 需要在开发的 REST 接口前加上 `/api/resource` 前缀。 比如开发的 REST 接口前缀是 `/v1/auth/method/adminSayHello`， 那么接口测试的URL为: `/api/resource/v1/auth/method/adminSayHello`.

## 后续工作

您可以使用CodeArts `部署镜像`、`流水线`等功能将上述构建、部署过程实现自动化，在代码变更的时候，一键式触发应用部署更新。 本文不再描述相关步骤。 

 
