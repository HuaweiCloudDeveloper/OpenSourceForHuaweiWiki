# 为体验项目扩展功能
体验项目 [OpenSourceForHuaweiDemoNodejs](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoNode/overview) ,主要提供了基础功能，对具体的业务，并没有提供相关代码。为了让大家更好的体验Demo，接下来给大家几个接口（参数参照验证结果演示

## 扩展过程
- 路由：参照routes
- 数据模型：参照model,可以DIY创建schema，或者直接注入初始化
- 中间件：待完善，可以自行安装meddleware，在控制器引入使用
- 日志：待完善，
- 控制器：参照controller
- 数据库连接：参照connect

## 验证结果

参照如下方式写一个接口进行验证
获取一个用户
![](./assets//result/getone.PNG) 
新增一个用户
![](./assets//result/postone.PNG) 
获取所有用户
![](./assets//result/result-getall.PNG) 