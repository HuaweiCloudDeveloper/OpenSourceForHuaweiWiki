



# 体验项目介绍
体验项目基于 [OpenSourceForHuaweiDemoNode](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoNode/overview) , 它使用 Express 框架做 RESTful 服务，数据层使用 pg/可以拓展其他ORM例如sequelize 连接 GaussDB 数据库。

Express 是一个流行的 Web 框架，它让构建web应用变得快捷且简单。

Sequelize 允许开发者使用 JavaScript 或 TypeScript 以面向对象的方式与数据库进行交互。它可以将数据库表映射为 JavaScript 对象，使得开发者可以通过操作对象来执行数据库操作，如查询、插入、更新和删除数据。

华为云数据库 GaussDB 是华为自主创新研发的分布式关系型数据库。该产品具备企业级复杂事务混合负载能力，同时支持分布式事务，同城跨AZ部署，数据0丢失，支持1000+的扩展能力，PB级海量存储。同时拥有云上高可用，高可靠，高安全，弹性伸缩，一键部署，快速备份恢复，监控告警等关键能力。

## 关于GaussDB 的说明
[设置加密模式](./assets/setmd5.PNG)

因为GaussDB的用户密码默认加密方式和PostgresDB的不一样，需要修改GaussDB的`password_encryption_type`为1（SHA256和MD5兼容模式），但是这样会降低GaussDB的安全性。

## 下载和本地运行示例项目
这里介绍如何在本地运行示例项目。您也可以直接参考 在华为云部署示例项目 。

git clone https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoNode.git

cd OpenSourceForHuaweiDemoNode

初始化数据库：

在华为云 GaussDB 数据库进入管理页面创建数据库（记住DBName）

配置数据库：
在`.env`中填写数据库相关信息
```javascript

DB_USER = USERNAME
DB_PASSWORD = DBPASSWORD
DB_HOST = YOURHOST
DB_DATABASE = DATABASENAME
DB_PORT = YOURDBPORT


PORT = YourServicePORT
```
 # 这只是一个示例，方便快速体验
```

运行示例：
进入项目目录
```bash
cd OpenSourceForHuaweiDemoNODE
```
启动Service服务（注意要在项目文件夹cmd）
```bash
npm run dev
```
如果启动时报错，提示还没有初始化，可以使用如下命令进行安装
```bash
npm install
```
偶然出现npm版本问题可以升级11版本
指令：npm install npm@11.0.0

如果出现数据链接异常‘SHA256’则需要修改数据库加密等级
[设置加密模式](./assets/setmd5.PNG)