# 为体验项目扩展功能

下面介绍在不了解ServiceComb Java Chassis和Open Tiny的情况下，给示例项目快速增加代码完成REST接口开发和简单界面的步骤。 如果需要详细了解技术背景，可以参考：

* [ServiceComb Java Chassis开发指南及最佳实践](https://huaweicse.github.io/servicecomb-java-chassis-doc/java-chassis/zh_CN/best-practise/about-fence.html)
* [Open Tiny Pro开发指南及最佳实践](https://opentiny.design/vue-pro/docs/start)

## 在resource-server新增加一个REST接口

* 在resource-server-api声明一个REST接口，比如：

```
@RequestMapping(path = "/v1/auth/method")
public interface PreMethodAuthService {
  @PostMapping(path = "/adminSayHello")
  String adminSayHello(String name);

  @PostMapping(path = "/guestSayHello")
  String guestSayHello(String name);

  @PostMapping(path = "/guestOrAdminSayHello")
  String guestOrAdminSayHello(String name);

  @PostMapping(path = "/everyoneSayHello")
  String everyoneSayHello(String name);
}
```

* 在resource-server里面实现该接口功能：

```
@RestSchema(schemaId = "PreMethodAuthEndpoint", schemaInterface = PreMethodAuthService.class)
public class PreMethodAuthEndpoint implements PreMethodAuthService {
  @Override
  @PreAuthorize("hasRole('ADMIN')")
  public String adminSayHello(String name) {
    return name;
  }

  @Override
  @PreAuthorize("hasRole('USER')")
  public String guestSayHello(String name) {
    return name;
  }

  @Override
  @PreAuthorize("hasAnyRole('USER','ADMIN')")
  public String guestOrAdminSayHello(String name) {
    return name;
  }

  @Override
  public String everyoneSayHello(String name) {
    return name;
  }
}
```

## 验证 REST 接口

开发好REST接口后，可以使用"接口测试"功能，测试REST接口的正确性。 

![](assets/demo-rest.png)

在任务验收的时候，需要提供 REST 接口测试结果的截图。 

> 注意： URL 需要在开发的 REST 接口前加上 `/api/resource` 前缀。 比如开发的 REST 接口前缀是 `/v1/auth/method/adminSayHello`， 那么接口测试的URL为: `/api/resource/v1/auth/method/adminSayHello`. 

## 在admin-website新增加一个菜单

开发者如果想自己开发一些复杂的界面，验证比较丰富的测试结果。可以参考下面的步骤开发界面。 通常的验收，只需要开发者开发REST接口，并通过接口测试页面反馈REST接口的调用结果，不需要开发自定义界面。 完成界面开发后，需要使用 `npm run build` 编译源代码，并拷贝到 `static` 目录， admin-website 使用 tomcat 托管静态页面。 当然也可以使用 `npm run start` 直接启动 nodejs 服务器，不启动 admin-website。 这种场景 `edge-servivce` 无法从 `zookeeper` 发现，需要修改 `WebsiteConfiguration` 文件，配置 `admin-website` 的地址信息。 

* 添加菜单路径
    /router/routes/modules 增加菜单描述文件 `examples.ts`。 

```
import { RoleType } from '@/types/roleType';
export default {
  path: 'examples',
  name: 'Examples',
  id: 'Examples',
  label: 'Examples',
  component: () => import('@/views/examples/index.vue'),
  meta: {
    locale: 'menu.examples',
    requiresAuth: true,
    order: 8,
    roles: [],
  },
  children: [
    {
      path: 'testMethodAuth',
      name: 'TestMethodAuth',
      id: 'TestMethodAuth',
      label: 'TestMethodAuth',
      component: () => import('@/views/examples/testMethodAuth/index.vue'),
      meta: {
        locale: 'menu.examples.testMethodAuth',
        requiresAuth: true,
        roles: [],
      },
    },
  ],
};
```

   修改/components/menu/index.vue注册菜单信息和图标

```
  ...
  const iconFiletext = IconFiletext();
  ...
  const routerTitle = [
    ...
    {
      value: 'Examples',
      name: 'menu.examples',
      icon: iconFiletext,
      bold: 'main-title',
    },
    {
      value: 'TestMethodAuth',
      name: 'menu.examples.testMethodAuth',
      icon: null,
      bold: 'title',
    },
    ...
```

* 添加页面

  增加/views/examples/index.vue

```
<template>
  <router-view />
</template>

<script lang="ts" setup></script>
```

  增加/views/examples/testMethodAuth/index.vue

```
<template>
  <div class="container">
    <div class="contain">
      <Breadcrumb :items="['menu.examples', 'menu.examples.testMethodAuth']" />
      <Main></Main>
    </div>
  </div>

  <div class="main">
    <div class="main-title">
      <span></span>
      <p>{{ $t('examples.testMethodAuth.authTest') }}</p>
    </div>

    <div class="test">
      <tiny-button type="primary"  @click="handleAdminOnly">{{ $t('test.method.auth.adminOnly') }}
      </tiny-button>
      <tiny-button type="primary"  @click="handleGuestOnly">{{ $t('test.method.auth.guestOnly') }}
      </tiny-button>
      <tiny-button type="primary"  @click="handleAdminOrGuest">{{ $t('test.method.auth.adminOrGuest') }}
      </tiny-button>
      <tiny-button type="primary"  @click="handleEveryOne">{{ $t('test.method.auth.everyone') }}
      </tiny-button>
    </div>
  </div>
</template>


<script lang="ts" setup>
  import axios from 'axios';
  import { inject, ref, reactive, computed } from 'vue';
  import {
    Button as TinyButton,
    Modal,
    Notify,
  } from '@opentiny/vue';

  function handleAdminOnly() {
    axios.post('/api/resource/v1/auth/method/adminSayHello?name=bob', {})
      .then(response => {
              Modal.message({
                   message: `${response}`,
                   status: 'success',
               });
            });
  };

  function handleGuestOnly() {
    axios.post('/api/resource/v1/auth/method/guestSayHello?name=bob', {})
      .then(response => {
              Modal.message({
                   message: `${response}`,
                   status: 'success',
               });
            });
  };

  function handleAdminOrGuest() {
    axios.post('/api/resource/v1/auth/method/guestOrAdminSayHello?name=bob', {})
      .then(response => {
              Modal.message({
                   message: `${response}`,
                   status: 'success',
               });
            });
  };

  function handleEveryOne() {
    axios.post('/api/resource/v1/auth/method/everyoneSayHello?name=bob', {})
      .then(response => {
              Modal.message({
                   message: `${response}`,
                   status: 'success',
               });
            });
  };
</script>
```

* 添加国际化资源

   增加 /views/examples/locale/zh-CN.ts 

```
export default {
  'menu.examples': '示例项目',
  'menu.examples.testMethodAuth': '用户权限测试',
  'examples.testMethodAuth.authTest': '用户权限测试',
  'test.method.auth.adminOnly': 'Admin有权限',
  'test.method.auth.guestOnly': 'Guest有权限',
  'test.method.auth.adminOrGuest': 'Admin和Guest有权限',
  'test.method.auth.everyone': '所有人都有权限',
};
```

   增加 /views/examples/locale/en-US.ts 

```
export default {
    'menu.examples': 'Examples Project',
    'menu.examples.testMethodAuth': 'User Authorization Test',
    'examples.testMethodAuth.authTest': 'User Authorization Test',
    'test.method.auth.adminOnly': 'Admin Only',
    'test.method.auth.userOnly': 'User Only',
    'test.method.auth.adminOrUser': 'Admin Or User',
    'test.method.auth.everyone': 'Every One',
};
```

   修改 /locale/zh-CN.ts 注册资源
```
...

import localeExamples from '@/views/examples/locale/zh-CN';
...
export default {
  ...localeExamples,
};
...
```

   修改 /locale/en-US.ts 注册资源
```
...

import localeExamples from '@/views/examples/locale/en-US';
...
export default {
  ...localeExamples,
};
...
```

