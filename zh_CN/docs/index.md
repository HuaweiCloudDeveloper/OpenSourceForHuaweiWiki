# 开源for Huawei介绍

[开源for Huawei](https://developer.huaweicloud.com/programs/opensource/contributing/) 围绕鲲鹏、昇腾、鸿蒙、华为云等技术生态，通过和公司、高校、社区的开发者合作，完成开源软件的适配开发、解决方案验证，让开源软件能够更加平滑、高效的运行于华为云上。

详细流程参考 [开源共创流程指引](open-source-work-flow.md) 。 

