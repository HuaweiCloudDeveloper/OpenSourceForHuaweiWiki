# 资源开通和规格信息

## 资源规格和版本

### 开源软件版本

默认适配开源软件的最新版本，代码合并到主干分支。

适配GaussDB的场景，如果需要提交代码到开源社区，默认采用开源驱动，即Open Gauss的驱动（JDBC、ODBC等）。DEMO中使用开源驱动或者商业驱动不做限制。

### GaussDB版本说明

| 产品名称         | 产品类型 | 数据库引擎版本  | 内核引擎版本  | 实例类型 | 部署形态 | 备注  |
|--------------|------|----------|---------|------|------|-----|
| 云数据库 GaussDB | 基础版  | V2.0-8.* | 505.1.* | 集中式  | 1主2备 | 推荐  |
| 云数据库 GaussDB | 企业版  | V2.0-8.* | 505.1.* | 分布式版 | 独立部署 |     |

### 容器版本说明

| 产品名称      | 集群类型             | 集群版本  | 集群规模 | 备注  |
|-----------|------------------|-------|------|-----|
| 云容器引擎 CCE | CCE Turbo 集群     | V1.29 | 50   | 推荐  |
| 云容器引擎 CCE | CCE Standard 集群  | V1.29 | 50   |     |

### ECS版本说明

| 产品名称   | CPU架构 | 	实例类型     | 公共镜像                 | 镜像版本                                         | 备注  |
|--------|-------|-----------|----------------------|----------------------------------------------|-----|
| 弹性云服务器 | 鲲鹏计算  | 鲲鹏通用计算增强型 | Huawei Cloud EulerOS | Huawei Cloud EulerOS 2.0 标准版 64位 ARM版(10GiB) | 推荐  |

### GaussDB开源贡献驱动版本


| 语言   | 名称                           | 版本       | 备注                                           |
|------|------------------------------|----------|----------------------------------------------|
| Java | org.opengauss:opengauss-jdbc | 5.1.0-og | 目前GaussDB未提供中央仓可获取的JDBC驱动，暂时使用OpenGauss提供的驱动 |


### 微服务引擎CSE

| 产品名称      | 引擎名称      | 注册配置中心类型 | 选择实例数 | 版本       |
|-----------|-----------|----------|-------|----------|
| CSE注册配置中心 | nacos-xxx | Nacos    | 500   | 2.1.0.11 |

## 资源购买指引

### 创建虚拟私有云（VPC）和子网

步骤1. 进入[VPC控制台](https://console.huaweicloud.com/vpc/?locale=zh-cn#/vpc/vpcs/list)创建虚拟私有云

步骤2. 选择所需的区域等信息，创建VPC和子网

步骤3. 可在VPC控制台查看VPC列表

可参考：[详情](https://support.huaweicloud.com/usermanual-vpc/zh-cn_topic_0013935842.html)

### 创建安全组

步骤1. 进入[安全组控制台](https://console.huaweicloud.com/console/?locale=zh-cn#/vpc/secGroups/list)创建安全组

步骤2. 选择所需区域，配置入/出方向规则，创建安全组

步骤3. 可在安全组控制台查看安全组列表

可参考：[详情](https://support.huaweicloud.com/usermanual-vpc/zh-cn_topic_0013748715.html)

### 创建弹性云服务器（ECS）

步骤1. 进入[ECS控制台](https://console.huaweicloud.com/ecm/?locale=zh-cn#/ecs/manager/vmList)购买弹性云服务器

步骤2. 选择所需区域和计费模式，配置参数，规格为“c7.large.2”，操作系统为“Huawei Cloud EulerOS”，创建ECS实例

步骤3. 可在ECS控制台查看ECS实例列表和登录ECS

可参考：[详情](https://support.huaweicloud.com/usermanual-ecs/ecs_03_7002.html)

### 创建云数据库GaussDB

步骤1. 进入[GaussDB控制台](https://console.huaweicloud.com/gaussdb/?locale=zh-cn#/openGauss/management/list)购买数据库实例

步骤2. 选择所需区域和计费模式，填写并选择实例相关信息，创建GaussDB实例

步骤3. 可在GaussDB控制台查看数据库实例列表和登录数据库

可参考：[详情](https://support.huaweicloud.com/usermanual-gaussdb/gaussdb_01_080.html)

### 创建云容器引擎（CCE）

步骤1. 进入[CCE控制台](https://console.huaweicloud.com/cce2.0/?locale=zh-cn#/cce/cluster/list)购买集群

步骤2. 选择所需区域和计费模式，填写并选择实例相关信息，创建CCE集群

步骤3. 可在CCE控制台查看集群列表和登录集群

可参考：[详情](https://support.huaweicloud.com/usermanual-cce/cce_10_0028.html)

### 创建微服务引擎（CSE）
 步骤1. 进入[微服务引擎控制台](https://console.huaweicloud.com/cse2) 确认授权
 
 步骤2. 进入注册配置中心，选择所需区域和计费模式，填写并选择实例相关信息，创建注册配置中心实例

 步骤3. 可在微服务引擎控制台查看注册配置中心实例列表

可参考：[详情](https://support.huaweicloud.com/usermanual-cse/cse_03_0001.html)
