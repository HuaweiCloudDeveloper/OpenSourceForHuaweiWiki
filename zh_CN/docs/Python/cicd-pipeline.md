# 在华为云部署体验项目
本文介绍如何使用 CodeArts 完成示例项目的构建与部署，实现持续交付。本文采用的部署方式为 CCE 部署，适用于容器化部署场景。如果您希望使用传统软件包部署方法，请参考 使用 [CodeArts 快速搭建基于 ECS 部署的代码开发流水线](https://support.huaweicloud.com/qs-devcloud/devcloud_qs_0001.html) 。

本文以部署 [OpenSourceForHuaweiDemoPython](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoPython/overview) 为例，描述如何使用 CodeArts 部署示例项目。

## 前提条件
请参考 [适配版本说明](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiWiki/blob/main/zh_CN/docs/resource-versions.md) 准备好资源， 参考 [示例项目说明](./intro-samples.md) 初始化数据库。

## 新建项目
项目是使用 CodeArts 各服务的基础，创建项目后才能完成后续操作。

打开 `软件开发生产线（CodeArts）` -> `前往工作台` -> `新建项目` 。
选择 `Scrum`，输入项目名称 `OpenSourceForHuaweiDemoPython`。

## 构建并推送镜像
通过编译构建任务将软件的源代码编译成镜像，并把镜像推送归档到容器镜像服务（SWR）中。

打开 `持续交付` -> `编译构建` -> `新建任务`，配置构建任务。
![](./assets/demo-create1.PNG)
![](./assets/demo-create2.PNG)

下一步
![](./assets/demo-create3.PNG)
构建环境配置
![](./assets/demo-create4.PNG)
制作镜像配置
![](./assets/demo-crteate5.PNG)
完成配置，单击“保存并执行”。等待任务执行完毕，在镜像仓库会生成1个服务镜像。镜像内容可以通过 `容器镜像服务` -> `我的镜像` 进行查看。
![](./assets/demo-docker-image.PNG)

## 创建负载
在云容器引擎（CCE）中创建无状态负载（Deployment），部署服务。
- 进入 `云容器引擎（CCE) 控制台`，点击准备工作中已经购买的集群。
![](./assets/demo-cce1.PNG)
- 进入详情页。
![](./assets/demo-cce2.PNG)
- 进入 `工作负载` -> `创建工作负载`。
![](./assets/demo-cce3.PNG)
- `容器配置` -> `选择镜像`先前上传到SWR上的镜像
![](./assets/demo-cce4.PNG)
- 在`容器配置` -> `环境变量`配置好镜像所需的环境变量
![](./assets/demo-cce5.PNG)

- 工作负载基础信息
| 配置类别 | 配置项  | 值 |
|----|------------|------|
| 基本信息 | 负载类型 | 无状态负载    |
| 基本信息 | 负载名称  | 具体名称   |
| 基本信息 | 实例数量  | 1  |
| 容器配置 | 基本信息-镜像名称  | 从我的镜像选择具体镜像  |
| 容器配置 | 基本信息-更新策略   | 总是拉取镜像  |
| 容器配置 | 基本信息-镜像版本  | v1.1  |
| 容器配置 | 环境变量-DB_NAME  | 填写GaussDB的数据库名称 |
| 容器配置 | 环境变量-DB_USER  | GaussDB的用户名 |
| 容器配置 | 环境变量-DB_PASSWORD | GaussDB 密码  |
| 容器配置 | 环境变量-DB_HOST | GaussDB IP地址  |

## 创建服务
- 在CCE详情页的左侧`服务` -> `创建服务`
![](./assets/demo-cce6.PNG)
- 配置服务相关信息
![](./assets/demo-cce7.PNG)
- 配置好后，可以通过`无状态负载`下的 日志 进行查看——服务是否正常
![](./assets/demo-cce8.PNG)

## 查看部署结果和进行接口验证
打开新的浏览器页面，输入 http://{IP}:8888/api/users/ 其中IP为ELB负载均衡的公网地址。访问会出现如下界面：
![](./assets/demo-result.PNG)

## 后续工作

您可以使用CodeArts `部署镜像`、`流水线`等功能将上述构建、部署过程实现自动化，在代码变更的时候，一键式触发应用部署更新。 本文不再描述相关步骤。 
