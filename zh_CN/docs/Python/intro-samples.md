# 体验项目介绍
体验项目基于 [OpenSourceForHuaweiDemoPython](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoPython/overview) , 它使用 Django 框架做 RESTful 服务，数据层使用 Django ORM 连接 GaussDB 数据库。

Django 是一个流行的 Python Web 框架，它让构建网站变得快速且简单。Django 自带了很多内置功能，比如用户管理、数据库操作等，这样开发者就不需要从头开始编写这些复杂的代码。

Django ORM 是 Django 的一个部分，它允许开发者用 Python 对象来表示数据库中的数据。通过 Django ORM，你可以像操作普通 Python 对象一样来处理数据库记录，而不需要写复杂的 SQL 代码。这让数据库操作变得更加直观和安全。简而言之，Django ORM 让数据库编程变得更简单。

华为云数据库 GaussDB 是华为自主创新研发的分布式关系型数据库。该产品具备企业级复杂事务混合负载能力，同时支持分布式事务，同城跨AZ部署，数据0丢失，支持1000+的扩展能力，PB级海量存储。同时拥有云上高可用，高可靠，高安全，弹性伸缩，一键部署，快速备份恢复，监控告警等关键能力。

## 关于GaussDB 驱动的说明
[获取GaussDB的驱动](https://support.huaweicloud.com/centralized-devg-v8-gaussdb/gaussdb-42-1836.html)
[psycopg2包安装教程](https://support.huaweicloud.com/centralized-devg-v8-gaussdb/gaussdb-42-0176.html)
因为GaussDB的用户密码默认加密方式和PostgresDB的不一样，使用pip提供的psycopg2驱动包，需要修改GaussDB的`password_encryption_type`为1（SHA256和MD5兼容模式），但是这样会降低GaussDB的安全性，因此，建议使用GaussDB提供的驱动包。

## 下载和本地运行示例项目
这里介绍如何在本地运行示例项目。您也可以直接参考 在华为云部署示例项目 。

git clone https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoPython.git

cd OpenSourceForHuaweiDemoPython

初始化数据库：

在华为云 GaussDB 数据库进入管理页面创建数据库（记住DBName）

配置数据库：
在`user-service/settings.py`中填写数据库相关信息
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'YourDBName',    # 数据库名称
        'USER': 'YourUsername',    # 数据库用户
        'PASSWORD': 'YourPassword',  # 用户密码
        'HOST': 'YourIP',   # GaussDB 主机
        'PORT': 'YourPort',        # GaussDB 端口
    }
}
```
编写视图方法：
`framework_core/views.py`文件
```python
# 列出所有 User（GET）
def get(self, request, *args, **kwargs):
    # pass
    return JsonResponse([{"name": "michael"}], safe=False, status=200)  # 这只是一个示例，方便快速体验
```

运行示例：
进入项目目录
```bash
cd OpenSourceForHuaweiDemoPython
```
启动Django服务（注意要在manage.py的同级目录执行命令）
```bash
python manage.py runserver 0.0.0.0:8000
```
如果启动Django时报错，提示还没有安装Django，可以使用如下命令进行安装
```bash
pip install django==3.2.25
```
