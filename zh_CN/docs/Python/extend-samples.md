# 为体验项目扩展功能
体验项目 [OpenSourceForHuaweiDemoPython](https://gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoPython/overview) ,主要提供了基础功能，对具体的业务，并没有提供相关代码。为了让大家更好的体验Demo，接下来给大家新增一个新增接口（RESTful API-POST method）

## 扩展过程
- 路由系统：因Demo已默认处理好，故不用额外操作
- 模型类（framework_core/models.py）:
```python
from django.db import models


class User(models.Model):
    username = models.CharField(max_length=150, unique=True)  # 用户名，唯一
    age = models.PositiveIntegerField()  # 年龄，正整数

    def __str__(self):
        return self.username

``` 
> 温馨提示：如果GaussDB上还没有创建数据表，请执行如下命令对数据库进行迁移操作。

a. `python manage.py makemigrations`：生成记录模型变化的迁移文件，描述对数据库结构的修改（如创建表、添加字段等）。
```bash
python manage.py makemigrations
```
b. `python manage.py migrate`：执行迁移文件，将其中描述的数据库更改实际应用到数据库中。
```bash
python manage.py migrate
```
- 视图类（framework_core/views.py）：
```python
from django.views import View
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from .models import User


@method_decorator(csrf_exempt, name='dispatch')
class UserView(View):
    # 创建 User（POST）
    def post(self, request, *args, **kwargs):
        username = request.POST.get("username")
        age = request.POST.get("age")

        if username and age:
            user = User.objects.create(username=username, age=age)
            # 返回成功的响应
            return JsonResponse({
                "message": "User created successfully",
                "id": user.id,
                "username": user.username,
                "age": user.age
            }, status=201)
        return JsonResponse({"error": "Username and age are required."}, status=400)


```
> 注意：此代码只作为Demo演示使用，在后续适配开发过程中，请根据实际场景，开发代码，此Demo仅供参考。

- 启动服务
```bash
python manage.py runserver 0.0.0.0:8000
```

- 通过Postman查看新增的RESTful API效果

![](./assets/demo-post-result.PNG)


