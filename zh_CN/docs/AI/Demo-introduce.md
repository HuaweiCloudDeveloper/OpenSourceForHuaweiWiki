# **VLLM体验项目介绍**

此项目致力于在昇腾AI处理器上优化并部署VLLM，实现高效的推理性能。项目关键在于集成了VLLM提供的API服务器，该服务器兼容OpenAI接口，极大地简化了开发者集成VLLM强大功能的过程。

昇腾AI处理器以其卓越的AI计算能力著称，通过CANN（Compute Architecture for Neural Networks）框架进一步优化了模型性能，使得VLLM能够在昇腾平台上发挥最佳性能。

VLLM是一个高效且易于使用的推理和服务库，专注于提供强大的视觉和语言模型，以理解和生成与图像相关的描述性文本。项目中部署的API服务兼容OpenAI，进一步简化了开发者的使用流程，提高了VLLM部署和应用的灵活性与便捷性。总体来看，该项目不仅显著提升了VLLM在昇腾平台上的推理效率，也为开发者提供了一个易于操作的AI服务接口。

体验项目部署流程：

![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/b2f2a951-8270-4e39-8aea-bf82bbc718fe/image.png 'image.png')





