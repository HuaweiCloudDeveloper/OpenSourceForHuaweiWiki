# 在云堡垒机部署体验项目

本文将介绍如何使用昇腾910b完成项目的构建和部署。采用的是在堡垒机上实现容器内部署。

以部署vllm项目为例，描述如何在昇腾堡垒机中部署vllm项目，并演示推理。

## 前提条件
**·资源规格信息:**

| 产品名称 | NPU架构 |操作系统  |
|--|--|--|
| 云堡垒机 | Ascend 910B3 | Huawei Cloud EulerOS 2.0（aarch64） |

**·Miniconda:**

下载软件包：
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-aarch64.sh

赋上可执行权限：

```chmod +x Miniconda3-latest-Linux-aarch64.sh```

执行安装：

```./Miniconda3-latest-Linux-aarch64.sh```

配置环境变量：

```export PATH="/root/miniconda3/bin:$PATH"  ```

```source ~/.bashrc```


## 搭建项目环境
**以下步骤是基于具备基础裸机环境之上**

**1、昇腾环境**

**·安装昇腾依赖包**

NPU上需要CANN、torch_npu、pytorch

CANN的安装参考：
`
https://www.hiascend.com/zh/developer/download/community/result?module=cann

·torch_npu的安装参考：

https://github.com/Ascend/pytorch/blob/master/README.zh.md`

**2、vllm环境**

**· 创建虚拟环境 vllm310并激活**


`Conda create -n vllm310 python=3.10  -y`

`Conda activate vllm310`


![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/a7114077-8eb0-4f00-ace7-afa7d33c6c45/image.png 'image.png')

<span style="color:#e60000;">**注:python版本为3.10**</span>

**· 拉取代码仓库**

git clone -b npu_support https://github.com/wangshuai09/vllm.git
![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/40a6fd0a-ecc6-4f89-ae95-c384f168a3f2/image.png 'image.png')

注：clone仓库，不要拉取最新的代码，否则安装会失败。下载支持npu安装的源码。

**· 安装vllm**

```VLLM_TARGET_DEVICE=npu pip install -e .```

![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/5462974a-3ac1-44b7-912b-6e84e2da9061/image.png 'image.png')

**安装完成回显**

![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/c0858721-2d07-42ac-bf66-b71b40a47074/image.png 'image.png')

<span style="color:#e60000;">**·补充：
执行“安装vllm时”会默认安装torch_npu 2.5.1，需降低torch_npu、torch版本为2.4.1</span>

`pip install torch torch_npu**`

![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/1d9eb739-3ca5-4a9d-b451-0d8fcdb06ff9/image.png 'image.png')
![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/e6e568d2-4a93-4093-99b2-1354b5966d86/image.png 'image.png')

## 离线推理

修改 examples/offline_inference_npu.py 第29行，选择Qwen1.5-4B-Chat模型

![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/68846eeb-3c67-406b-920d-cfcbb289aaf8/image.png 'image.png')

可能报错modelscope找不到

解决方法：

`pip install modelscope`

运行程序：自动下载模型 Qwen1.5-4B-Chat

![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/0aeed376-4414-42ae-a1c3-48a7435882ac/image.png 'image.png')

推理结果

![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/61537393-b83c-4e92-ab6c-dabb770673ab/image.png 'image.png')



## 推理部署
项目提供了一个兼容 OpenAI 接口的 API 服务器，使得开发者可以像使用 OpenAI 服务一样，轻松地集成 VLLM 的能力。
在这里我们直接将vLLM部署为模仿 OpenAI API协议的服务器，这使得vLLM可以用作使用OpenAI API的应用程序的直接替代品。

通过命令启动服务器：

`python -m vllm.entrypoints.openai.api_server `

--help  加上此参数可查看命令行参数的详细信息。

![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/88079cc5-7d02-44fb-9734-85230b42b14e/image.png 'image.png')

开始部署

1、输入以下命令启动服务器

`python -m vlLm.entrypoints.openai.api_server --model "/root/.cache/ Modelscope
/hub/Qwen/Qwen1 .5- 4B-Chat”-- served-model-name " openchat "`

--model为本地模型的路径，--served-model-name是模型在服务器中的别名，这里我们取名简称openchat,其他都是默认值，当然也可以自己设置。

![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/078511d8-80f8-43c4-af1a-42ae8f31bd0d/image.png 'image.png')

显示以下信息

![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/f18dcc47-f6f2-465b-a22e-e1a468331a8f/image.png 'image.png')

服务已经启动成功


2、通过API访问

OpenAI 提供了 Completions 和 ChatCompletions 两个相关的 API，两个API很相似，你可以使用其中的任意一个，在这里我们选择Completions。

Completions说明：服务器启动成功后，我们可以在shell中发送一个请求进行测试，该请求使用了OpenAIChat API查询模型，请求内容如下：


    curl http://localhost:8000/v1/completions \
    -H "Content-Type: application/json" \
    -d '{
        "model": "openchat",
        "prompt": "San Francisco is a",
        "max_tokens": 7,
        "temperature": 0
    }'
    
<span style="color:#e60000;">说明：</span>

<span style="color:#e60000;">-d 里面是请求的参数，可以自己设置；</span>

<span style="color:#e60000;">temperature 为 0，代表每次模型的回答都相同，不具有随机性，你可以自由调整参数来满足你的需求；</span>


<span style="color:#e60000;">Promote封装你输入的问题；</span>

如下：可以看到返回了结果，其中红色框中的是模型为我们续写的内容。

![image.png](https://raw.gitcode.com/hlc0101/vllmDemo/attachment/uploads/2113e55a-af57-499f-b907-6bda56cc607a/image.png 'image.png')
